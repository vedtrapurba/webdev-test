<?php

use Illuminate\Database\Seeder;
use App\Status;
class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('status_name' => 'created'),
            array('status_name' => 'invited'),
            array('status_name' => 'registered'),
        );

        Status::insert($data);
    }
}
