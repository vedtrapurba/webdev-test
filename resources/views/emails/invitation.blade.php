<!DOCTYPE html>
<html>
<head>
    <title>Verifikasi register</title>
     <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Your'e Invited</div>

                <div class="panel-body">
                    <p>
                    {!! $content !!}
                    </p>
                    
                        <p>Please Click <a href="{{ url('register/event/'.$token) }}">click</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>