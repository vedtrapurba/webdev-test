@extends('layouts.master')
@section('header')

  <title>Events</title>

@endsection

@section('body')
<h3 class="page-title">Email</h3>
					<div class="row">
						<div class="col-md-12">
							<!-- BASIC TABLE -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">List Email</h3>
                                </div>
                                
								<div class="panel-body">
                                    <a href ="{{ route('mail.add') }}"> <button type="button"  class="btn btn-default"><i class="fa fa-plus-square"></i> Add </button></a>
									<table class="table">
										<thead>
											<tr>
												<th>#</th>
												<th>subject</th>
												<th>content</th>
												<th>schedule</th>
												<th>Registration end</th>
											</tr>
										</thead>
										<tbody>
										@php
										$no = 1;
										@endphp
										@forelse($mail as $item)
											<tr>
												<td>{{ $no ++ }}</td>
												<td>{{$item->subject}}</td>
												<td>{{$item->content}}</td>
												<td>{{$item->schedule}}</td>
												<td>{{$item->end_time}}</td>
											</tr>
											@empty
											<tr>
												<td colspan="5">No records found</td>
											</tr>
										@endforelse
										</tbody>
									</table>
								</div>
							</div>
							
                        </div>
                    </div>
@endsection

@show