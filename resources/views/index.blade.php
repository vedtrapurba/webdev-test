@extends('layouts.master')
@section('header')

  <title>Email</title>

@endsection

@section('body')
<div class="card-body">
	@if (session('status'))
		<div class="alert alert-success" role="alert">
			{{ session('status') }}
		</div>
	@endif

	Welcome, You are logged in!
</div>
@endsection
@show