<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
<div class="row justify-content-center">
                    <div class="col-md-8 mt-5">
                        <div class="card">
                            <div class="card-header">Register</div>
                            <div class="card-body">

                                <form class="form-horizontal" method="post" action="{{ url('/register/event') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="name" class="cols-sm-2 control-label">Email</label>
                                        <div class="cols-sm-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                                <input type="text" class="form-control" name="email" id="email" value="{{$attendee->email}}" readonly />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="cols-sm-2 control-label">Your Name</label>
                                        <div class="cols-sm-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                                <input type="text" class="form-control" name="name" id="name" placeholder="Enter your Name" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="cols-sm-2 control-label">Date of birth</label>
                                        <div class="cols-sm-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                                <input type="date" class="form-control" name="birth_date" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="username" class="cols-sm-2 control-label">Gender</label>
                                        <div class="cols-sm-10">
                                            <div class="input-group">
                                                <select name="gender" class="form-control">
                                                    <option value="L">Laki-laki</option>
                                                    <option value="P">Peremuan</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="cols-sm-2 control-label">Designer Favorit</label>
                                        <div class="cols-sm-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                                <input type="text" class="form-control" name="designer_favorit" placeholder="designer favorit" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="token" value="{{$attendee->token}}">
                                    <div class="form-group ">
                                        <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Register</button>
                                    </div>

                                </form>
                            </div>

                        </div>
                    </div>
                </div>
</div>
<style>
    .ticket{
  display: flex;
  font-family:Roboto;
  margin: 16px;
  border: 1px solid $grey;
  position:relative;
  
  &:before{
    content:'';
    width:32px;
    height:32px;
    background-color:#fff;
    border: 1px solid $grey;
    border-top-color:transparent;
    border-left-color:transparent;
    position:absolute;
    transform:rotate(-45deg);
    left:-18px;
    top:50%;
    margin-top:-16px;
    border-radius:50%;
  }
  
  &:after{
    content:'';
    width:32px;
    height:32px;
    background-color:#fff;
    border: 1px solid $grey;
    border-top-color:transparent;
    border-left-color:transparent;
    position:absolute;
    transform:rotate(135deg);
    right:-18px;
    top:50%;
    margin-top:-16px;
    border-radius:50%;
  }
  
  &--start{
    position:relative;
    &:before{
      content:'';
      width:32px;
      height:32px;
      background-color:#fff;
      border: 1px solid $grey;
      border-top-color:transparent;
      border-left-color:transparent;
      border-right-color:transparent;
      position:absolute;
      transform:rotate(-45deg);
      left:-18px;
      top:-2px;
      margin-top:-16px;
      border-radius:50%;
    }
    
    &:after{
      content:'';
      width:32px;
      height:32px;
      background-color:#fff;
      border: 1px solid $grey;
      border-top-color:transparent;
      border-left-color:transparent;
      border-bottom-color:transparent;
      position:absolute;
      transform:rotate(-45deg);
      left:-18px;
      top:100%;
      margin-top:-16px;
      border-radius:50%;
    }
    & > img{
      display:block;
      padding: 24px;
      height: 270px;
    }
    border-right: 1px dashed $grey;
  }
  
  &--center{
    padding: 24px;
    flex: 1;
    &--row{
      display: flex;
      &:not(:last-child){
        padding-bottom:48px;
      }
      
      &:first-child{
        span{
          color:$primary;
          text-transform:uppercase;
          line-height:24px;
          font-size:13px;
          font-weight:500;
        }
        
        strong{
          font-size:20px;
          font-weight:400;
          text-transform:uppercase;
        }
      }
    }
    
    &--col{
      display: flex;
      flex:1;
      width: 50%;
      box-sizing:border-box;
      flex-direction: column;
      &:not(:last-child){
        padding-right: 16px;
      }
    }
  }
  
  &--end{
    padding: 24px;
    background-color:$primary;
    display:flex;
    flex-direction:column;
    position:relative;
    &:before{
      content:'';
      width:32px;
      height:32px;
      background-color:#fff;
      border: 1px solid $grey;
      border-top-color:transparent;
      border-right-color:transparent;
      border-bottom-color:transparent;
      position:absolute;
      transform:rotate(-45deg);
      right:-18px;
      top:-2px;
      margin-top:-16px;
      border-radius:50%;
    }
    
    &:after{
      content:'';
      width:32px;
      height:32px;
      background-color:#fff;
      border: 1px solid $grey;
      border-right-color:transparent;
      border-left-color:transparent;
      border-bottom-color:transparent;
      position:absolute;
      transform:rotate(-45deg);
      right:-18px;
      top:100%;
      margin-top:-16px;
      border-radius:50%;
    }
  
    & > div{
      &:first-child{
      flex:1;
        & >img{
          width: 128px;
          padding: 4px;
          background-color: #fff;
        }
      }
      
      &:last-child{
        > img{
          display:block;
          margin: 0 auto;
          filter: brightness(0) invert(1);
          opacity:0.64;
        }
      }
    }
  }
  
  &--info{
    &--title{
      text-transform:uppercase;
      color: #757575;
      font-size:13px;
      line-height:24px;
      font-weight:600;
      white-space:nowrap;
      overflow:hidden;
      text-overflow:ellipsis;
    }
    
    &--subtitle{
      font-size: 16px;
      line-height:24px;
      font-weight:500;
      color:$primary;
      white-space:nowrap;
      overflow:hidden;
      text-overflow:ellipsis;
    }
    
    &--content{
      font-size:13px;
      line-height:24px; 
      font-weight: 500;
      white-space:nowrap;
      overflow:hidden;
      text-overflow:ellipsis;
    }
  }
}
    </style>