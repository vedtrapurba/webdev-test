@extends('layouts.master')
@section('header')

  <title>Events</title>
  <script src="{{ asset('js/lib/utils.js') }}"></script>
  <script src="{{ asset('js/lib/emails-input.js') }}"></script>
  <script src="{{ asset('js/app.js') }}"></script>
@endsection

@section('body')
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">Add email</h3>
    </div>
    <div class="panel-body">
        <form action="{{ url('/invitation') }}" method="POST">
            {{ @csrf_field() }}
        <label>Subject</label>
        <input type="text" class="form-control" name="subject" placeholder="subject" required="required">
        <br>
        <label>Content</label>
        <textarea name="content" class="form-control" placeholder="enter body" required="required"></textarea>
        <br>
        <label>Schedule</label> (<small id="emailHelp" class="form-text text-muted" required="required">tanggal email akan dikirim</small>)
        <input type="datetime-local" class="form-control" name="schedule" required="required">
       
        <br>   
        <label>End Registration</label>
        <input type="datetime-local" class="form-control" name="end_time" required="required">
        <br>   
        <label>Recipients</label> (<small id="emailHelp" class="form-text text-muted">input email and enter</small>)
        <div id="emails-input" required="required"></div>
        <br>
        <button type="submit" class="btn btn-primary">submit</button>
        </form>
    </div>
</div>
@endsection

@show
