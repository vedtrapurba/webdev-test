@extends('layouts.master')
@section('header')

  <title>Events</title>

@endsection

@section('body')
<h3 class="page-title">Attendee</h3>
					<div class="row">
						<div class="col-md-12">
							<!-- BASIC TABLE -->
							<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">List Attende</h3>
                                </div>
                                
								<div class="panel-body">
                                    
									<table class="table">
										<thead>
											<tr>
												
												<th>Mail subject</th>
												<th>Email</th>
                                                <th>Name</th>
                                                <th>Registration code</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
									
										@forelse($attendees as $item)
											<tr>
												
												<td>{{$item->mail->subject}}</td>
												<td>{{$item->email}}</td>
                                                <td>{{$item->name}}</td>
                                                <td>{{$item->registration_code}}</td>
												<td>{{$item->status->status_name}}</td>
											</tr>
											@empty
											<tr>
												<td colspan="5">No records found</td>
											</tr>
										@endforelse
										</tbody>
									</table>
								</div>
							</div>
							
                        </div>
                    </div>
@endsection

@show