<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    protected $fillable = ['mail_id','email','status_id','token'];

    function mail()
    {
        return $this->belongsTo('App\Mail');
    }

    function status()
    {
        return $this->belongsTo('App\Status');
    }
}
