<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail;
use App\Invitation;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class InvitationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function email() 
    {
        $mail = Mail::orderBy('created_at', 'DESC')->get();
        $this->data['mail'] = $mail;
        return view('email',$this->data);
    }

    public function attendee() 
    {
        $attendees = Invitation::orderBy('created_at', 'DESC')->get();
        $this->data['attendees'] = $attendees;
        return view('attendee',$this->data);
    }

    public function addEmail()
    {
        return view('form_email');
    }

    public function createInvitation(Request $request){
        $params = $request->except('_token');

        \DB::transaction(
			function () use ($params) {
				$email = $this->saveEmail($params);
				$this->saveEmailAttendee($email,$params);
			}
		);
        return redirect('email');
    }

    public function saveEmail($params)
    {
        $mailParams = [
            'subject'=> $params['subject'],
            'content'=> $params['content'],
            'schedule'=> $params['schedule'],
            'end_time'=> $params['end_time'],
		];

		return Mail::create($mailParams);
    }

    public function saveEmailAttendee($email,$params)
    {   
        $attendee = $params['recipients'];
        foreach($attendee as $item) {	
				$attendeeParams = [
					'mail_id' => $email->id,
                    'email'   => $item,
                    'status_id' => 1,
                    'token'   => $this->generateRandomString(30),
				];

				Invitation::create($attendeeParams);
				
            }
            
    }
    
    public function generateRandomString($x, $uppercase = false, $unique = false, $prefix = '')
    {
        if ($unique) {
            $string = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $x);
        } else {
            $string = Str::random($x);
        }
        if ($uppercase) {
            $string = strtoupper($string);
        }
        if ($prefix != '') {
            $string = $prefix.$string;
        }
        return $string;
    }

    public function showFrom($token)
    {
        $attendee = Invitation::where('token',$token)->first();
        if($attendee->status_id == 3){
            return redirect('code-registration/'. $attendee->registration_code);
        } else{
            $this->data['attendee'] = $attendee;
            return view('form_register',$this->data);
        }
       
    }

    public function saveForm(Request $request)
    {
        $invitation = Invitation::where('token', $request->token)->firstOrFail();
        $invitation->name = $request->name;
        $invitation->birth_date = $request->birth_date;
        $invitation->gender = $request->gender;
        $invitation->designer_favorit = $request->designer_favorit;
        $invitation->registration_code = $this->generateRandomString(20,$uppercase = false, $unique = false, $prefix="EVENT-");
        $invitation->status_id = 3;
        $invitation->save();

        return redirect('code-registration/'. $invitation->registration_code);
    }

    public function codeRegistration($code)
    {
        $code = Invitation::where('registration_code',$code)->first();
        $this->data['code'] = $code;
        return view('registration_code',$this->data);
    }
}
