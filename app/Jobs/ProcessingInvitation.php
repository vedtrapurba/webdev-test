<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail;
use Illuminate\Support\Facades\Mail as Mails;
use App\Mail\SendEmailInvitation;
use DB, Log, DateTime;
class ProcessingInvitation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $invitation; 

    public function __construct($invitation)
    {
        //
        $this->invitation = $invitation;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $start = microtime(true);
            $micro = sprintf("%06d", ($start - floor($start)) * 1000000);
            $d = new DateTime(date('Y-m-d H:i:s.'.$micro, $start));
            $d = $d->format("Y-m-d H:i:s.u");
            $start_time = "Start of email campaign: $d";
            
            $recipients = DB::table('invitations')->where('mail_id', $this->invitation->id)->get();                       
            $mail = Mail::find($this->invitation->id);    

            foreach($recipients as $recipient) {
                $message = new SendEmailInvitation($mail, $recipient);
                                      
                Mails::to($recipient->email)->send($message);      
                DB::table('invitations')->where('email','=',$recipient->email)->update(['status_id' =>2]);                    
            }
               
            $end = microtime(true);
            $micro = sprintf("%06d", ($end - floor($end)) * 1000000);
            $d = new DateTime(date('Y-m-d H:i:s.'.$micro, $end));
            $d = $d->format("Y-m-d H:i:s.u");
            $end_time = "End of email campaign: $d";
            
            $duration = $end - $start;
            $hours = (int)($duration/60/60);
            $minutes = (int)($duration/60)-$hours*60;
            $seconds = (int)$duration;
            $miliseconds = (double)$duration-$seconds-$hours*60*60-$minutes*60;
            
            $executionTime ="Execution Time ".$seconds.".".substr($miliseconds,2)." second(s)";
            Log::info("\n".$start_time."\n".$end_time."\n".$executionTime);
            dump('['.date('Y-m-d h:i:s', time()).'] : '. $start_time);
            dump('['.date('Y-m-d h:i:s', time()).'] : '. $end_time);
            dump('['.date('Y-m-d h:i:s', time()).'] : '. $executionTime);
        } catch (\Exception $e) {
            Log::error( $e->getMessage() );
            dump('['.date('Y-m-d h:i:s', time()).'] : '.$e->getMessage());
        }
    }
}
