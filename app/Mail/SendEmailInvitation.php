<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Mail;
class SendEmailInvitation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $mail;
    protected $recipient;
    public function __construct(Mail $mail,$recipient)
    {
        $this->mail = $mail;
        $this->token = $recipient->token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.invitation')
                    ->subject($this->mail->subject)                    
                    ->with([        
                    'content'=>$this->mail->content,
                    'token' =>$this->token,
                    ]);
    }       
}
