<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mail extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['subject','content','schedule','end_time'];

    public function invitation()
	{
		return $this->hasMany('App\Invitation');
    }
}
