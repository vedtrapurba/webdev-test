<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\SendEmailThankyou;
use DB;

class thankyouemail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thankyou:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dump('['.date('Y-m-d h:i:s', time()).'] : Start of Email');
        $invitation = DB::table('invitations')->where('status_id', 3)->where('is_thankyou','0')->where('updated_at','<',\Carbon\Carbon::parse('-10 hours'))->get();
        if ($invitation) {
            dump('pass');
            $job = (new SendEmailThankyou($invitation));
            dispatch($job);

        } else {
            dump('['.date('Y-m-d h:i:s', time()).'] : No Active email!');
        }
        dump('['.date('Y-m-d h:i:s', time()).'] : End of Email');
    }
    
}
