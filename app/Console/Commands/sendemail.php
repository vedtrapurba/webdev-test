<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\ProcessingInvitation;
use DB;

class sendemail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendemail:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dump('['.date('Y-m-d h:i:s', time()).'] : Start of Email');
        $invitation = DB::table('mails')->where('status', 'Pending')->where('schedule', '<=', \Carbon\Carbon::now())->first();
        
        if ($invitation) {
           
            // Start processing
            DB::table('mails')->where('id', $invitation->id)->update(['status' => 'Processing']);
            $job = (new ProcessingInvitation($invitation));
            dispatch($job);

            // Finish Email 
            DB::table('mails')->where('id', $invitation->id)->update(['status' => 'Processed']);
        } else {
            dump('['.date('Y-m-d h:i:s', time()).'] : No Active email!');
        }
        dump('['.date('Y-m-d h:i:s', time()).'] : End of Email');
    }
}
