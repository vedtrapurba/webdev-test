<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/register/event/{token}', 'InvitationController@showFrom');
Route::get('/code-registration/{code}', 'InvitationController@codeRegistration');
Route::post('/register/event', 'InvitationController@saveForm');
Auth::routes();

Route::middleware(['auth'])->group(function () {
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/email', 'InvitationController@email')->name('mail.index');
Route::get('/attendee', 'InvitationController@attendee')->name('attendee.index');
Route::get('/add-email', 'InvitationController@addEmail')->name('mail.add');
Route::post('/invitation', 'InvitationController@createInvitation');

});